# README #

### What is this repository for? ###
This repository contains all the polish translate files (format po and mo) for a shop named Arcavias, which is accessable as a plugin TYPO3.

Files have been translated for Arcavias ver. 1.2

Installation is very simply. Copy all files to a catalog 
arcavias/vendor/arcavias/arcavias-core

If you have any suggestions, proposals, objections etc. please don't hesitate to contact me!